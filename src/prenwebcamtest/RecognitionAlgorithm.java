/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prenwebcamtest;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Simon
 */
public class RecognitionAlgorithm {

    private KorbLocation location;
    private BufferedImage img;
    private boolean debug;

    private int minKorbWidth, minKorbHeight, smoothFactor, peakLo, peakHi, maxKorbLuminancy;

    public RecognitionAlgorithm(boolean debug) {
        this.img = null;
        this.location = new KorbLocation();
        this.debug = debug;
        minKorbWidth = 90;
        minKorbHeight = 100;
        smoothFactor = 1;
        peakLo = 50;
        peakHi = 60;
        maxKorbLuminancy = 90;
    }

    public RecognitionAlgorithm(int minKorbWidth, int minKorbHeight, int smoothFactor, int peakLo, int peakHi, int maxKorbLuminancy) {
        this.img = null;
        this.location = null;
        this.minKorbWidth = minKorbWidth;
        this.minKorbHeight = minKorbHeight;
        this.smoothFactor = smoothFactor;
        this.peakLo = peakLo;
        this.peakHi = peakHi;
        this.maxKorbLuminancy = maxKorbLuminancy;
    }

    public KorbLocation recognize(BufferedImage img) {

        this.location = new KorbLocation();
        this.img = img;

        searchHorizontal(img.getHeight() / 2);//Row
        searchVertical(location.getHorizontalMitte()); //Column

        return location;

    }

    private int[] search(int[] pixelData, int minDistance) {

        pixelData = smoothArray(pixelData, smoothFactor);

        //Now the Pixels have been read.
        int korbStartPixel = 0;
        int korbEndPixel = 0;

        maxKorbLuminancy = (int) ((float) getAverage(pixelData) * 0.4f);

        for (int i = 0; i < pixelData.length - 5; i++) {
            if (pixelData[i + 5] - pixelData[i] < (-peakLo)) {
                if (averageOfNextValues(pixelData, i, minDistance) <= maxKorbLuminancy) {
                    korbStartPixel = i;
                    break;
                }
            }
        }

        //Fehlerbehandlung;
        if ((korbStartPixel + minDistance) < pixelData.length - 5) {

            for (int i = korbStartPixel + minDistance; i < pixelData.length - 5; i++) {
                //Search the first Peak where the Luminancy rises by 40
                if (Math.abs(pixelData[i + 5] - pixelData[i]) > peakHi) {
                    korbEndPixel = i + 6;
                    if (korbEndPixel > pixelData.length)
                    {
                        korbEndPixel = pixelData.length;
                    }
                    break;
                }
            }
        }

        return new int[]{korbStartPixel, korbEndPixel};
    }

    private void searchVertical(int column) {

        int[] pixelData = getColumnOfPixel(img, column);
        int[] result = search(pixelData, minKorbHeight);

        location.setVerticalStart(result[0]);
        location.setVerticalEnd(result[1]);
    }

    private void searchHorizontal(int row) {

        int[] pixelData = getRowOfPixel(row);

        int[] result = search(pixelData, minKorbWidth);

        location.setHorizontalStart(result[0]);
        location.setHorizontalEnd(result[1]);

        if (debug) {
            try {
                PrintWriter writer = new PrintWriter("horizontal.csv", "UTF-8");
                for (int i = 0; i < pixelData.length; i++) {
                    writer.println(pixelData[i]);
                }
                writer.close();
            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                Logger.getLogger(RecognitionAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private int getAverage(int[] pixelData) {
        int sum = 0;
        for (int i = 0; i < pixelData.length; i++) {
            sum += pixelData[i];
        }
        return sum / pixelData.length;
    }

    private int[] getRowOfPixel(int row) {

        int[] pixelData = new int[img.getWidth()];
        //Get the Row of Pixels
        for (int i = 0; i < pixelData.length; i++) {

            int[] currentPixel = getPixelData(i, row);

            pixelData[i] = currentPixel[0] + currentPixel[1] + currentPixel[2];
        }

        return pixelData;
    }

    private int[] getColumnOfPixel(BufferedImage img, int column) {

        int[] pixelData = new int[img.getHeight()];
        //Get the Row of Pixels
        for (int i = 0; i < pixelData.length; i++) {

            int[] currentPixel = getPixelData(column, i);

            pixelData[i] = currentPixel[0] + currentPixel[1] + currentPixel[2];
        }

        return pixelData;
    }

    private int[] getPixelData(int x, int y) {
        int argb = img.getRGB(x, y);

        int rgb[] = new int[]{
            (argb >> 16) & 0xff, //red
            (argb >> 8) & 0xff, //green
            (argb) & 0xff //blue
        };
        return rgb;
    }

    private int[] smoothArray(final int[] array, int factor) {
        //Duplikat vom pixelData Array erstellen fürs Glätten
        if (factor == 1) {
            return array;
        }
        int[] orgPixels = array.clone();

        //Glätten
        for (int i = factor; i < array.length; i++) {

            int sumOfValues = 0;
            for (int k = 1; k <= factor; k++) {
                sumOfValues += orgPixels[i - k];
            }

            array[i] = (sumOfValues) / factor;
        }

        return array;
    }

    private int averageOfNextValues(int[] array, int start, int distance) {
        int sum = 0;
        if ((start + distance) > array.length) {
            distance = array.length - start;
        }
        for (int k = start; k < (start + distance); k++) {
            sum += array[k];
        }
        return sum / distance;
    }
}
