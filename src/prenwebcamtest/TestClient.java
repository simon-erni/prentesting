/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prenwebcamtest;

import com.github.sarxos.webcam.Webcam;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 *
 * @author Simon
 */
public class TestClient implements Runnable {

    private Webcam webcam;
    private BufferedImage currentImage;
    private JFrame imageViewerWindow;
    private ImageViewer imageViewer;
    private RecognitionAlgorithm erkenner;
    private KorbLocation currentLocation;
    private boolean debug;

    public TestClient(boolean debug) {
        erkenner = new RecognitionAlgorithm(debug);
        this.debug = debug;

    }

    public void refreshWebcamImage() {
        if (webcam == null) {
            webcam = Webcam.getWebcams().get(1);
            webcam.setViewSize(new Dimension(640, 480));
            webcam.open();
        }
        currentImage = webcam.getImage();
    }

    public void refreshStaticImage() {
        try {
            currentImage = ImageIO.read(new File(""
                    + "C:/Users/Simon/Documents/NetBeansProjects/"
                    + "PRENWebcamTest/build/classes/prenwebcamtest/bild.jpg"));
        } catch (IOException ex) {
            Logger.getLogger(TestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void drawCurrentKorbLocationOnCurrentImageAsSquare() {
        if (currentImage != null && currentLocation != null) {
            Graphics g2d = currentImage.createGraphics();
            g2d.setColor(Color.red);
            g2d.drawRect(
                    currentLocation.getHorizontalStart(),
                    currentLocation.getVerticalStart(),
                    currentLocation.getHorizontalEnd() - currentLocation.getHorizontalStart(),
                    currentLocation.getVerticalEnd() - currentLocation.getVerticalStart());
        }
    }

    public void drawCurrentKorbLocationOnCurrentImageAsFilledSquare() {
        if (currentImage != null && currentLocation != null) {
            Graphics g2d = currentImage.createGraphics();
            g2d.setColor(Color.red);
            g2d.fillRect(
                    currentLocation.getHorizontalStart(),
                    currentLocation.getVerticalStart(),
                    currentLocation.getHorizontalEnd() - currentLocation.getHorizontalStart(),
                    currentLocation.getVerticalEnd() - currentLocation.getVerticalStart());
        }
    }

    public void drawCurrentKorbLocationOnCurrentImageAsLine() {
        if (currentImage != null && currentLocation != null) {
            Graphics g2d = currentImage.createGraphics();
            g2d.setColor(Color.red);
            g2d.drawLine(currentLocation.getHorizontalStart(), currentImage.getHeight(), currentLocation.getHorizontalStart(), 0);
            g2d.drawLine(currentLocation.getHorizontalEnd(), currentImage.getHeight(), currentLocation.getHorizontalEnd(), 0);
        }
    }

    public void showCurrentImage(int width, int height) {
        if (imageViewerWindow == null) {
            imageViewerWindow = new JFrame("PREN Korberkenner 2000");
            imageViewer = new ImageViewer(scaleImage(currentImage, width, height));
            imageViewerWindow.add(imageViewer);
            imageViewerWindow.setLocationRelativeTo(null);
            imageViewerWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            imageViewerWindow.pack();
            imageViewerWindow.setSize(new Dimension(640, 480));
            imageViewerWindow.setLocation(100, 100);

            imageViewerWindow.setVisible(true);
        }
        imageViewer.refreshImage(currentImage);
    }

    private BufferedImage scaleImage(BufferedImage image, int width, int height) {
        if (width > 0 && height > 0) {
            Image tmp = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);

            BufferedImage scaledBufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

            Graphics2D graphics = scaledBufferedImage.createGraphics();
            graphics.drawImage(tmp, 0, 0, null);
            graphics.dispose();
            return scaledBufferedImage;
        } else {
            throw new RuntimeException("Wrong Parameters for Width and Height");
        }
    }

    public static void main(final String[] args) throws IOException, Exception {

        TestClient tester = new TestClient(false);
        new Thread(tester).start();

    }

    @Override
    public void run() {
        while (debug == false) {
            refreshWebcamImage();
            currentLocation = erkenner.recognize(currentImage);
            drawCurrentKorbLocationOnCurrentImageAsLine();
            showCurrentImage(640, 480);
        }
    }
}
