package prenwebcamtest;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.*;

public class ImageViewer extends JPanel {

    private static final long serialVersionUID = 1L;
    private BufferedImage image;

    public ImageViewer(BufferedImage image) {
        setSize(new Dimension(640, 480));

        this.image = image;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();
        //Paint it on screen
        g2d.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        g2d.dispose();
    }

    public void refreshImage(BufferedImage img) {
        this.image = img;
        repaint();
    }

}
