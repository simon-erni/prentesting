/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prenwebcamtest;

/**
 *
 * @author Simon
 */
public class KorbLocation {

    private int horizontalStart;
    private int horizontalEnd;
    private int verticalStart;
    private int verticalEnd;

    public KorbLocation() {
        horizontalEnd = 0;
        horizontalStart = 0;
        verticalEnd = 0;
        verticalStart = 0;
    }

    public int getHorizontalStart() {
        return horizontalStart;
    }

    public void setHorizontalStart(int horizontalStart) {
        this.horizontalStart = horizontalStart;
    }

    public int getHorizontalEnd() {
        return horizontalEnd;
    }

    public void setHorizontalEnd(int horizontalEnd) {
        this.horizontalEnd = horizontalEnd;
    }

    public int getVerticalStart() {
        return verticalStart;
    }

    public void setVerticalStart(int verticalStart) {
        this.verticalStart = verticalStart;
    }

    public int getVerticalEnd() {
        return verticalEnd;
    }

    public void setVerticalEnd(int verticalEnd) {
        this.verticalEnd = verticalEnd;
    }

    public int getHorizontalMitte() {
        return (horizontalEnd - horizontalStart) / 2 + horizontalStart;
    }

    public int getVerticalMitte() {
        return (verticalEnd - verticalStart) / 2 + verticalStart;
    }

    @Override
    public String toString()
    {
        String returnString = "";
        returnString += "Horizontal Start: " + horizontalStart + "\n";
        returnString += "Horizontal End: " + horizontalEnd + "\n";
        returnString += "Vertical Start: " + verticalStart + "\n";
        returnString += "Vertical End: " + verticalEnd + "\n";
        
        return returnString;
    }
}
